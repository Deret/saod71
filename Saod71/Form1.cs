﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Saod71
{
    public partial class Form1 : Form
    {
        KeyVal[] x;
        int tt;
        public Form1()
        {
            InitializeComponent();
        }
        public int GetRadioIndex(GroupBox group)
        {
            foreach (Control control in group.Controls)
                if (control is RadioButton)
                    if (((RadioButton)control).Checked)
                        return int.Parse(control.Tag.ToString());
            return -1;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var n = Convert.ToInt32(textBox2.Text);
            var rnd = new System.Random();
            var init = Enumerable.Range(0, n).OrderBy(b => rnd.Next()).ToArray();
            x = new KeyVal[n];
            for (int i = 0; i < init.Length; i++)
            {
                x[i] = new KeyVal(i, init[i]);
            }

            tt = x[0].val;
            x[n / 2].val = x[0].val;
            x[n - 1].val = x[0].val;
            chart1.Series[0].Points.Clear();
            chart1.ChartAreas[0].AxisX.Minimum = -1;
            chart1.ChartAreas[0].AxisX.Maximum = n;
            for (int i = 0; i != n; i++)
            {
                chart1.Series[0].Points.AddXY(i, x[i].val);
                chart1.Series[0].Points[i].AxisLabel = x[i].key.ToString();

                if(x[i].val == tt)
                    chart1.Series[0].Points[i].Color = Color.Red;
            }
        }

        private KeyVal[] BubbleSort(KeyVal[] x)
        {
            KeyVal temp = new KeyVal();
            for (int i = 0; i < x.Length; i++)
            {
                for (int j = 0; j < x.Length - 1; j++)
                {
                    if (x[j].val > x[j + 1].val)
                    {
                        temp = x[j + 1];
                        x[j + 1] = x[j];
                        x[j] = temp;
                    }
                }
            }
            return x;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            switch (GetRadioIndex(groupBox1))
            {
                case 3:
                    Array.Sort(x,(a,b)=>a.val.CompareTo(b.val));
                    break;
                case 0:
                    x=BubbleSort(x);
                    break;
                default:
                    break;
            }

            chart1.Series[0].Points.Clear();
            chart1.ChartAreas[0].AxisX.Minimum = -1;
            chart1.ChartAreas[0].AxisX.Maximum = x.Length;
            for (int i = 0; i != x.Length; i++)
            {
                chart1.Series[0].Points.AddXY(i, x[i].val);
                chart1.Series[0].Points[i].AxisLabel = x[i].key.ToString();

                if (x[i].val == tt)
                    chart1.Series[0].Points[i].Color = Color.Red;
            }
          //  chart1.Series[0].Points[2].Color=Color.Red;
        }
    }
}
